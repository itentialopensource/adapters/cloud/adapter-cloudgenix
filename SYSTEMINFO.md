# Clougenix

Vendor: Palo Alto
Homepage: https://www.paloaltonetworks.com/

Product: Clougenix
Product Page: https://www.paloaltonetworks.com/sase/sd-wan

## Introduction
We classify Clougenix (now known as Prisma SD-WAN) into the Cloud domain as Clougenix provides Cloud Services. 

"Cloud-delivered service that implements app-defined, autonomous SD-WAN to help you secure and connect your branch offices, data centers and large campus sites without increasing cost and complexity" 

The Clougenix adapter can be integrated to the Itential Device Broker which will allow your Devices to be managed within the Itential Configuration Manager Application. 

## Why Integrate
The Clougenix adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Clougenix. With this adapter you have the ability to perform operations such as:

- Manage and optimize bandwidth

## Additional Product Documentation
The [API documents for Clougenix](https://pan.dev/sdwan/api/)