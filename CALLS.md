## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.


### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cloudgenix. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cloudgenix.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Adapter for Cloudgenix. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getTenant(tenantId, callback)</td>
    <td style="padding:15px">get tenants</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTenant(tenant, callback)</td>
    <td style="padding:15px">create tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSites(tenantId, siteId, callback)</td>
    <td style="padding:15px">get sites</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWANInterfaces(tenantId, siteId, callback)</td>
    <td style="padding:15px">get wan interfaces for site</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites/{pathv2}/waninterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWANInterface(tenantId, siteId, interfaceParam, callback)</td>
    <td style="padding:15px">create wan interface for site</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites/{pathv2}/waninterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSite(tenantId, site, callback)</td>
    <td style="padding:15px">create site in tenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteLinkHealth(tenantId, query, callback)</td>
    <td style="padding:15px">get site and link health</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getElements(tenantId, elementId, callback)</td>
    <td style="padding:15px">get elements</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/elements/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getElementInterfaces(tenantId, siteId, elementId, callback)</td>
    <td style="padding:15px">get element interfaces</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites/{pathv2}/elements/{pathv3}/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createElementInterface(tenantId, siteId, elementId, interfaceParam, callback)</td>
    <td style="padding:15px">create element interface</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites/{pathv2}/elements/{pathv3}/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getElementInterfaceStatus(tenantId, siteId, elementId, interfaceId, callback)</td>
    <td style="padding:15px">get element interface status</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites/{pathv2}/elements/{pathv3}/interfaces/{pathv4}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getElementSNMPConfiguration(tenantId, siteId, elementId, callback)</td>
    <td style="padding:15px">get Element SNMP Configuration</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites/{pathv2}/elements/{pathv3}/snmpagents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createElement(tenantId, element, callback)</td>
    <td style="padding:15px">create element</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/elements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWANs(tenantId, callback)</td>
    <td style="padding:15px">get wan networks</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/wannetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWAN(tenantId, wan, callback)</td>
    <td style="padding:15px">create WAN network</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/wannetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLANs(tenantId, siteId, callback)</td>
    <td style="padding:15px">get LAN networks</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites/{pathv2}/lannetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLAN(tenantId, siteId, lan, callback)</td>
    <td style="padding:15px">create LAN network</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites/{pathv2}/lannetworks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationDefinition(tenantId, appId, callback)</td>
    <td style="padding:15px">get application definition</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/appdefs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySets(tenantId, callback)</td>
    <td style="padding:15px">get policy sets</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/policysets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicySet(tenantId, policySet, callback)</td>
    <td style="padding:15px">create policy set</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/policysets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicySetRules(tenantId, policySetId, callback)</td>
    <td style="padding:15px">get policy set rules</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/policysets/{pathv2}/policyrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicySetRule(tenantId, policySetId, policyRule, callback)</td>
    <td style="padding:15px">create policy set rules</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/policysets/{pathv2}/policyrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createApplication(tenantId, application, callback)</td>
    <td style="padding:15px">create application</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/appdefs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityZones(tenantId, callback)</td>
    <td style="padding:15px">get security zones</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/securityzones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSecurityZone(tenantId, securityZone, callback)</td>
    <td style="padding:15px">create security zone</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/securityzones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteSecurityZones(tenantId, siteId, callback)</td>
    <td style="padding:15px">get site security zones</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites/{pathv2}/sitesecurityzones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSiteSecurityZone(tenantId, siteId, securityZone, callback)</td>
    <td style="padding:15px">create site securityzone</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/sites/{pathv2}/sitesecurityzones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityPolicySets(tenantId, callback)</td>
    <td style="padding:15px">get security policy sets</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/securitypolicysets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSecurityPolicySet(tenantId, securityPolicySet, callback)</td>
    <td style="padding:15px">create security policy set</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/securitypolicysets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityPolicySetRules(tenantId, policySetId, callback)</td>
    <td style="padding:15px">get security policy set rules</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/securitypolicysets/{pathv2}/securitypolicyrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSecurityPolicySetRule(tenantId, policySetId, policyRule, callback)</td>
    <td style="padding:15px">create security policy set rule</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}/securitypolicysets/{pathv2}/securitypolicyrules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
