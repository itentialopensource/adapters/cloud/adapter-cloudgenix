
## 0.6.5 [10-15-2024]

* Changes made at 2024.10.14_20:58PM

See merge request itentialopensource/adapters/adapter-cloudgenix!18

---

## 0.6.4 [08-25-2024]

* turn off save metric

See merge request itentialopensource/adapters/adapter-cloudgenix!16

---

## 0.6.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cloudgenix!15

---

## 0.6.2 [08-14-2024]

* Changes made at 2024.08.14_19:19PM

See merge request itentialopensource/adapters/adapter-cloudgenix!14

---

## 0.6.1 [08-07-2024]

* Changes made at 2024.08.06_20:53PM

See merge request itentialopensource/adapters/adapter-cloudgenix!13

---

## 0.6.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!12

---

## 0.5.6 [03-28-2024]

* Changes made at 2024.03.28_13:23PM

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!11

---

## 0.5.5 [03-21-2024]

* Changes made at 2024.03.21_13:56PM

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!10

---

## 0.5.4 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!9

---

## 0.5.3 [03-11-2024]

* Changes made at 2024.03.11_15:36PM

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!8

---

## 0.5.2 [02-28-2024]

* Changes made at 2024.02.28_11:51AM

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!7

---

## 0.5.1 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!6

---

## 0.5.0 [12-14-2023]

* 2023 Adapter Migration

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!5

---

## 0.4.0 [11-29-2023]

* 2023 Adapter Migration

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!5

---

## 0.3.0 [05-18-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!4

---

## 0.2.0 [01-21-2022]

- migration to the latest foundation and broker ready
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!3

---

## 0.1.1 [07-25-2021]

- migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-cloudgenix!2

---
