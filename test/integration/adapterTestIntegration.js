/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cloudgenix',
      type: 'Cloudgenix',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Cloudgenix = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Cloudgenix Adapter Test', () => {
  describe('Cloudgenix Class Tests', () => {
    const a = new Cloudgenix(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cloudgenix-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cloudgenix-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let tenantTenantId = 'fakedata';
    const tenantCreateTenantBodyParam = {
      id: 'string',
      _etag: 3,
      _content_length: 'string',
      _schema: 10,
      _created_on_utc: 6,
      _updated_on_utc: 5,
      _status_code: 'string',
      _request_id: 'string',
      tenant_id: 'string',
      inactive: true,
      disabled: false,
      _created_by_operator_id: 'string',
      _updated_by_operator_id: 'string',
      canonical_name: 'string',
      name: 'string',
      password_policy: {},
      is_esp: true,
      is_support: true
    };
    describe('#createTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTenant(tenantCreateTenantBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal(8, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(8, data.response._schema);
                assert.equal(2, data.response._created_on_utc);
                assert.equal(2, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal('string', data.response.tenant_id);
                assert.equal(true, data.response.inactive);
                assert.equal(true, data.response.disabled);
                assert.equal('string', data.response._created_by_operator_id);
                assert.equal('string', data.response._updated_by_operator_id);
                assert.equal('string', data.response.canonical_name);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.password_policy);
                assert.equal(false, data.response.is_esp);
                assert.equal(true, data.response.is_support);
              } else {
                runCommonAsserts(data, error);
              }
              tenantTenantId = data.response.id;
              saveMockData('Tenant', 'createTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenant(tenantTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal(7, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(8, data.response._schema);
                assert.equal(1, data.response._created_on_utc);
                assert.equal(3, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal('string', data.response.tenant_id);
                assert.equal(false, data.response.inactive);
                assert.equal(true, data.response.disabled);
                assert.equal('string', data.response._created_by_operator_id);
                assert.equal('string', data.response._updated_by_operator_id);
                assert.equal('string', data.response.canonical_name);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.password_policy);
                assert.equal(false, data.response.is_esp);
                assert.equal(true, data.response.is_support);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenant', 'getTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteTenantId = 'fakedata';
    const siteCreateSiteBodyParam = {};
    describe('#createSite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSite(siteTenantId, siteCreateSiteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(7, data.response._schema);
                assert.equal(3, data.response._created_on_utc);
                assert.equal(3, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(3, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'createSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteSiteId = 'fakedata';
    const siteCreateWANInterfaceBodyParam = {};
    describe('#createWANInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createWANInterface(siteTenantId, siteSiteId, siteCreateWANInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(8, data.response._schema);
                assert.equal(6, data.response._created_on_utc);
                assert.equal(6, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(5, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'createWANInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteGetSiteLinkHealthBodyParam = {
      type: 'string',
      nodes: [
        {}
      ]
    };
    describe('#getSiteLinkHealth - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSiteLinkHealth(siteTenantId, siteGetSiteLinkHealthBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cloudgenix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'getSiteLinkHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSites - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSites(siteTenantId, siteSiteId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(2, data.response._schema);
                assert.equal(8, data.response._created_on_utc);
                assert.equal(3, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(10, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'getSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWANInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWANInterfaces(siteTenantId, siteSiteId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(3, data.response._schema);
                assert.equal(7, data.response._created_on_utc);
                assert.equal(5, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(7, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'getWANInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const elementTenantId = 'fakedata';
    const elementCreateElementBodyParam = {};
    describe('#createElement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createElement(elementTenantId, elementCreateElementBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(6, data.response._schema);
                assert.equal(4, data.response._created_on_utc);
                assert.equal(8, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(9, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Element', 'createElement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const elementElementId = 'fakedata';
    const elementSiteId = 'fakedata';
    const elementCreateElementInterfaceBodyParam = {};
    describe('#createElementInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createElementInterface(elementTenantId, elementSiteId, elementElementId, elementCreateElementInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(1, data.response._schema);
                assert.equal(1, data.response._created_on_utc);
                assert.equal(3, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(2, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Element', 'createElementInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getElements - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getElements(elementTenantId, elementElementId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(2, data.response._schema);
                assert.equal(10, data.response._created_on_utc);
                assert.equal(1, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(7, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Element', 'getElements', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getElementInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getElementInterfaces(elementTenantId, elementSiteId, elementElementId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(4, data.response._schema);
                assert.equal(6, data.response._created_on_utc);
                assert.equal(8, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(7, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Element', 'getElementInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const elementInterfaceId = 'fakedata';
    describe('#getElementInterfaceStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getElementInterfaceStatus(elementTenantId, elementSiteId, elementElementId, elementInterfaceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal(3, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(4, data.response._schema);
                assert.equal(1, data.response._created_on_utc);
                assert.equal(2, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal('string', data.response.region);
                assert.equal('string', data.response.element_id);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.negotiated_mtu);
                assert.equal('string', data.response.device);
                assert.equal(true, Array.isArray(data.response.ipv4_addresses));
                assert.equal(true, Array.isArray(data.response.ipv6_addresses));
                assert.equal('object', typeof data.response.dns_v4_config);
                assert.equal(true, Array.isArray(data.response.routes));
                assert.equal('string', data.response.operational_state);
                assert.equal('string', data.response.mac_address);
                assert.equal('object', typeof data.response.port);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Element', 'getElementInterfaceStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getElementSNMPConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getElementSNMPConfiguration(elementTenantId, elementSiteId, elementElementId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(2, data.response._schema);
                assert.equal(8, data.response._created_on_utc);
                assert.equal(10, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(5, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Element', 'getElementSNMPConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkTenantId = 'fakedata';
    const networkSiteId = 'fakedata';
    const networkCreateLANBodyParam = {};
    describe('#createLAN - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createLAN(networkTenantId, networkSiteId, networkCreateLANBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(5, data.response._schema);
                assert.equal(6, data.response._created_on_utc);
                assert.equal(6, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(8, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'createLAN', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkCreateWANBodyParam = {};
    describe('#createWAN - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createWAN(networkTenantId, networkCreateWANBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(7, data.response._schema);
                assert.equal(1, data.response._created_on_utc);
                assert.equal(4, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(3, data.response.count);
                assert.equal('string', data.response.type);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'createWAN', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLANs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLANs(networkTenantId, networkSiteId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(10, data.response._schema);
                assert.equal(1, data.response._created_on_utc);
                assert.equal(4, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(2, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'getLANs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWANs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWANs(networkTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(5, data.response._schema);
                assert.equal(10, data.response._created_on_utc);
                assert.equal(7, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(10, data.response.count);
                assert.equal('string', data.response.type);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Network', 'getWANs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyTenantId = 'fakedata';
    const policyCreateApplicationBodyParam = {};
    describe('#createApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createApplication(policyTenantId, policyCreateApplicationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(10, data.response._schema);
                assert.equal(3, data.response._created_on_utc);
                assert.equal(3, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(10, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'createApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyCreatePolicySetBodyParam = {};
    describe('#createPolicySet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPolicySet(policyTenantId, policyCreatePolicySetBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cloudgenix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'createPolicySet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyPolicySetId = 'fakedata';
    const policyCreatePolicySetRuleBodyParam = {};
    describe('#createPolicySetRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicySetRule(policyTenantId, policyPolicySetId, policyCreatePolicySetRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(8, data.response._schema);
                assert.equal(6, data.response._created_on_utc);
                assert.equal(6, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(5, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'createPolicySetRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policyAppId = 'fakedata';
    describe('#getApplicationDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplicationDefinition(policyTenantId, policyAppId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(4, data.response._schema);
                assert.equal(4, data.response._created_on_utc);
                assert.equal(7, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(5, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getApplicationDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicySets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPolicySets(policyTenantId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cloudgenix-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getPolicySets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicySetRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicySetRules(policyTenantId, policyPolicySetId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(6, data.response._schema);
                assert.equal(7, data.response._created_on_utc);
                assert.equal(9, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policy', 'getPolicySetRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firewallTenantId = 'fakedata';
    const firewallCreateSecurityPolicySetBodyParam = {};
    describe('#createSecurityPolicySet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSecurityPolicySet(firewallTenantId, firewallCreateSecurityPolicySetBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(9, data.response._schema);
                assert.equal(5, data.response._created_on_utc);
                assert.equal(9, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(7, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Firewall', 'createSecurityPolicySet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firewallPolicySetId = 'fakedata';
    const firewallCreateSecurityPolicySetRuleBodyParam = {};
    describe('#createSecurityPolicySetRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSecurityPolicySetRule(firewallTenantId, firewallPolicySetId, firewallCreateSecurityPolicySetRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(1, data.response._schema);
                assert.equal(6, data.response._created_on_utc);
                assert.equal(1, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(8, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Firewall', 'createSecurityPolicySetRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firewallCreateSecurityZoneBodyParam = {};
    describe('#createSecurityZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSecurityZone(firewallTenantId, firewallCreateSecurityZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(3, data.response._schema);
                assert.equal(1, data.response._created_on_utc);
                assert.equal(3, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Firewall', 'createSecurityZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firewallSiteId = 'fakedata';
    const firewallCreateSiteSecurityZoneBodyParam = {};
    describe('#createSiteSecurityZone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSiteSecurityZone(firewallTenantId, firewallSiteId, firewallCreateSiteSecurityZoneBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(3, data.response._schema);
                assert.equal(7, data.response._created_on_utc);
                assert.equal(8, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(4, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Firewall', 'createSiteSecurityZone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityPolicySets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityPolicySets(firewallTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(2, data.response._schema);
                assert.equal(2, data.response._created_on_utc);
                assert.equal(1, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(5, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Firewall', 'getSecurityPolicySets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityPolicySetRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityPolicySetRules(firewallTenantId, firewallPolicySetId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(10, data.response._schema);
                assert.equal(7, data.response._created_on_utc);
                assert.equal(10, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(2, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Firewall', 'getSecurityPolicySetRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecurityZones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecurityZones(firewallTenantId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(4, data.response._schema);
                assert.equal(6, data.response._created_on_utc);
                assert.equal(5, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(3, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Firewall', 'getSecurityZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteSecurityZones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteSecurityZones(firewallTenantId, firewallSiteId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response._etag);
                assert.equal('string', data.response._content_length);
                assert.equal(5, data.response._schema);
                assert.equal(2, data.response._created_on_utc);
                assert.equal(8, data.response._updated_on_utc);
                assert.equal('string', data.response._status_code);
                assert.equal('string', data.response._request_id);
                assert.equal(10, data.response.count);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Firewall', 'getSiteSecurityZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
